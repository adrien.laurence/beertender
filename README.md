# Notes

## Installation de l'application

```
cd beertender

pip install -r requirements.txt

python manage.py test

python manage.py migrate

python manage.py runserver
```

## Users

Voici les logs du superuser

    login = 'admin-oc'
    password = 'password-oc'

Voici les logs du user staff

    login = 'staff'
    password = 'staff'

Voici les logs du user standard

    login = 'user'
    password = 'user'

## Explications

J'ai rempli les tables dans la base SQLLite par les scripts de migrations.
Ces scripts ressemblent à un liquibase ce qui est pas mal mais je pense qu'il aurait fallu utiliser 
la fonction loaddata plutôt parce que le migrate est joué aussi dans les tests unitaires.
Il se trouve que j'ai gagné du temps sur les setup des tests mais je pense qu'il faut passer pr migrate 
pour les valeurs référentiels et loaddata pour les autres data

L'authentification est gérée par le module django 'django.contrib.auth', même si je pense 
qu'à terme on utilise le package JWT du DRF pour avoir le token d'authentification avec
le refresh token.

Les permissions sont gérées dans le fichier permissions.py qui a été créé. Il y a déjà un 
boolean 'is_staff' sur le User donc on l'utilise pour connaitre si le User est du staff.
Ensuite, on gère les permissions en fonction des méthodes de la requête (GET, POST, ...) .

Les tests unitaires sont faits pour cette partie dans le package tests.

On trie les résultats avec les OrderingFilter présent sur le ViewSet
On filtre les résultats avec les SearchFilter présent sur le ViewSet
La pagination se gère automatiquement en ajoutant dans le settings.py

    REST_FRAMEWORK = {
        'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
        'PAGE_SIZE': 20
    }

Le projet est organisé en package technique (views, models, serializers)

#### Les references

Fait en totalité

#### Les comptoirs du bar

Fait en totalité

#### Les stocks

Pas fait totalement, le résultat n'est pas une liste mais un objet avec un attribut stocks qui possède la liste

*Optionnel : Il faut pouvoir trier, filtrer et paginer les résultats.* Non fait

#### Classer les comptoirs

Fait en totalité

#### Le menu

Fait en totalité
