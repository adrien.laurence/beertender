from rest_framework.permissions import BasePermission

class IsAdminAuthenticated(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_superuser)


class BeerReferencePermission(BasePermission):

    def has_permission(self, request, view):
        safe_methods = ['GET', 'HEAD', 'OPTIONS']

        if not request.user.is_authenticated:
            return False

        if request.method in safe_methods:
            return True

        return bool(request.user.is_staff)


class IsStaffAuthenticated(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated and request.user.is_staff)


class IsAuthenticated(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated)
