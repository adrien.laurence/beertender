# Generated by Django 4.1.7 on 2023-04-06 20:24

from beertender_api.models.bar_counter import BarCounter
from django.db import migrations


def create_counters(apps, schema_editor):
    ref1 = BarCounter()
    ref1.name = "1er étage"
    ref1.save()
    ref2 = BarCounter()
    ref2.name = "2ème étage"
    ref2.save()

class Migration(migrations.Migration):

    dependencies = [
        ('beertender_api', '0005_barcounter'),
    ]

    operations = [
        migrations.RunPython(create_counters, migrations.RunPython.noop)
    ]
