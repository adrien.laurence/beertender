# Generated by Django 4.1.7 on 2023-04-06 20:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('beertender_api', '0004_auto_20230406_2204'),
    ]

    operations = [
        migrations.CreateModel(
            name='BarCounter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
    ]
