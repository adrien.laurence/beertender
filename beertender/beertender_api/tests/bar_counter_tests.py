from django.contrib.auth.models import User
from django.urls import reverse_lazy
from rest_framework.test import APITestCase


class BarCounterTest(APITestCase):
    url_list = reverse_lazy('bars-list')
    url_ranking = reverse_lazy('bars-ranking')
    url_detail = reverse_lazy('bars-detail')

    user = User("user", "user-test@test.com", "user", is_staff=False)
    staff = User("staff", "staff-test@test.com", "staff", is_staff=True)

    def test_authentication(self):
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 401)

    def test_permission_user(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(self.url_list,
                                    {'name': 'New Reference'})
        self.assertEqual(response.status_code, 403)

    def test_permission_staff(self):
        self.client.force_authenticate(user=self.staff)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(self.url_list,
                                    {'name': 'New Bar'})
        self.assertEqual(response.status_code, 201)

    def test_permission_staff(self):
        self.client.force_authenticate(user=self.staff)
        response = self.client.get(self.url_ranking)
        print(response.json())
        expected = [
            {
                "name": "all_stocks",
                "description": "Liste des comptoirs qui ont toutes les références en stock",
                "bars": []
            },
            {
                "name": "miss_at_least_one",
                "description": "Liste des comptoirs qui ont au moins une référence épuisée",
                "bars": [
                    1,
                    2
                ]
            }
        ]
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)


    def test_list(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        expected = [
            {
                "pk": 1,
                "name": "1er étage"

            },
            {
                "pk": 2,
                "name": "2ème étage"
            }
        ]

        self.assertEqual(response.json()['count'], 2)
        self.assertEqual(response.json()['results'], expected)
