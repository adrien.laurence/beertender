from django.urls import reverse_lazy
from rest_framework.test import APITestCase


class StockBarCounterTest(APITestCase):
    url_list = reverse_lazy('menu-list')

    def test_authentication(self):
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)

    def test_list(self):
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        excepted = [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                "available": "available"
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.",
                "available": "available"
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique.",
                "available": "available"
            }
        ]

        self.assertEqual(response.json()['count'], 3)
        self.assertEqual(response.json()['results'], excepted)

    def test_list_with_counter_id(self):
        response = self.client.get(self.url_list+"?bar_counter_id=1")
        self.assertEqual(response.status_code, 200)
        excepted = [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                "available": "available"
            },
            {
                "ref": "brewdogipa",
                "name": "Brewdog Punk IPA",
                "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.",
                "available": "outofstock"
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique.",
                "available": "available"
            }
        ]

        self.assertEqual(response.json()['count'], 3)
        self.assertEqual(response.json()['results'], excepted)

    def test_list_with_counter_id_and_is_available(self):
        response = self.client.get(self.url_list+"?bar_counter_id=1&is_available=True")
        self.assertEqual(response.status_code, 200)
        excepted = [
            {
                "ref": "leffeblonde",
                "name": "Leffe blonde",
                "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                "available": "available"
            },
            {
                "ref": "fullerindiapale",
                "name": "Fuller's India Pale Ale",
                "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique.",
                "available": "available"
            }
        ]

        self.assertEqual(response.json()['count'], 2)
        self.assertEqual(response.json()['results'], excepted)