from beertender_api.models import BeerReference
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from rest_framework.test import APITestCase


class StockBarCounterTest(APITestCase):
    url_list = reverse_lazy('stock-list')
    url_detail = reverse_lazy('stock-detail', kwargs={'pk':1})

    user = User("user", "user-test@test.com", "user", is_staff=False)
    staff = User("staff", "staff-test@test.com", "staff", is_staff=True)

    def test_authentication(self):
        response = self.client.get(self.url_detail)
        self.assertEqual(response.status_code, 401)

    def test_permission_user(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_detail)
        self.assertEqual(response.status_code, 200)

    def test_detail(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_detail)
        self.assertEqual(response.status_code, 200)
        excepted = {
            "stocks": [
                {
                    "ref": "leffeblonde",
                    "name": "Leffe blonde",
                    "description": "Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !",
                    "stock": 10
                },
                {
                    "ref": "brewdogipa",
                    "name": "Brewdog Punk IPA",
                    "description": "La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.",
                    "stock": 0
                },
                {
                    "ref": "fullerindiapale",
                    "name": "Fuller's India Pale Ale",
                    "description": "Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique.",
                    "stock": 15
                }
            ]
        }

        self.assertEqual(response.json(), excepted)
