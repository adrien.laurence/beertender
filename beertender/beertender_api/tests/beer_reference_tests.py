from beertender_api.models import BeerReference
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from rest_framework.test import APITestCase


class BeerReferenceTest(APITestCase):
    url_list = reverse_lazy('references-list')
    url_detail = reverse_lazy('references-detail')

    user = User("user", "user-test@test.com", "user", is_staff=False)
    staff = User("staff", "staff-test@test.com", "staff", is_staff=True)
    ref1 = BeerReference(ref='leffeblonde', name='Leffe blonde',
                         description="Une bière blonde d'abbaye brassée depuis 1240 et que l'on ne présente plus !")
    ref2 = BeerReference(ref='brewdogipa', name='Brewdog Punk IPA',
                         description="La Punk IPA est une bière écossaise s'inspirant des tendances américaines en matière de brassage et du choix des houblons.")
    ref3 = BeerReference(ref='fullerindiapale', name="Fuller's India Pale Ale",
                         description="Brassée pour l'export, la Fuller's India Pale Ale est la vitrine du savoir faire bien « british » de cette brasserie historique.")

    def test_authentication(self):
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 401)

    def test_permission_user(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(self.url_list,
                                    {'ref': 'newref', 'name': 'New Reference', 'description': 'A new beer reference'})
        self.assertEqual(response.status_code, 403)

    def test_permission_staff(self):
        self.client.force_authenticate(user=self.staff)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        response = self.client.post(self.url_list,
                                    {'ref': 'newref', 'name': 'New Reference', 'description': 'A new beer reference'})
        self.assertEqual(response.status_code, 201)

    def test_list(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url_list)
        self.assertEqual(response.status_code, 200)
        excepted = [
            {
                'ref': self.ref1.ref,
                'name': self.ref1.name,
                'description': self.ref1.description
            }, {
                'ref': self.ref2.ref,
                'name': self.ref2.name,
                'description': self.ref2.description
            }, {
                'ref': self.ref3.ref,
                'name': self.ref3.name,
                'description': self.ref3.description
            }
        ]

        self.assertEqual(response.json()['count'], 3)
        self.assertEqual(response.json()['results'], excepted)
