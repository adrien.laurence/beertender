from beertender_api import permissions
from beertender_api.models import Stock
from beertender_api.serializers import StockSerializer
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet


class StockViewSet(ModelViewSet):
    serializer_class = StockSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        queryset = Stock.objects.all()
        return queryset

