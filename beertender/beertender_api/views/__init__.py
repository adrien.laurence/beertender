from beertender_api.views.auth_view import *
from beertender_api.views.bar_counter_view import *
from beertender_api.views.beer_reference_view import *
from beertender_api.views.stock_view import *
from beertender_api.views.menu_view import *
