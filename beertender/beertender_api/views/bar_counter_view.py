from beertender_api import permissions
from beertender_api.models import BarCounter, Stock
from beertender_api.serializers import BarCounterSerializer, BarCounterStockSerializer
from django.db import transaction
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet


class BarCounterViewSet(ModelViewSet):
    serializer_class = BarCounterSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = ['name']
    search_fields = ['name']
    permission_classes = [permissions.BeerReferencePermission]

    def get_queryset(self):
        return BarCounter.objects.all()

    @transaction.atomic
    @action(detail=False, methods=['get'])
    def ranking(self, request):
        all_stocks = {"name": "all_stocks",
                      "description": "Liste des comptoirs qui ont toutes les références en stock",
                      "bars": []}

        miss_at_least_one = {"name": "miss_at_least_one",
                             "description": "Liste des comptoirs qui ont au moins une référence épuisée",
                             "bars": []}

        bar_counters = BarCounter.objects.all()

        for counter in bar_counters:
            stocks = Stock.objects.filter(bar_counter=counter, stock__lte=0)
            if len(stocks) > 0:
                miss_at_least_one["bars"].append(counter.pk)
            else:
                all_stocks["bars"].append(counter.pk)
        return Response([all_stocks, miss_at_least_one])


class BarCounterStockViewSet(ReadOnlyModelViewSet):
    serializer_class = BarCounterStockSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return BarCounter.objects.all()
