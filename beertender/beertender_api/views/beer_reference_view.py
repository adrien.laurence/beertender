from beertender_api import permissions
from beertender_api.models import BeerReference
from beertender_api.serializers import BeerReferenceSerializer
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet


class BeerReferenceViewSet(ModelViewSet):
    serializer_class = BeerReferenceSerializer
    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = ['ref', 'name', 'description']
    search_fields = ['ref', 'name', 'description']
    permission_classes = [permissions.BeerReferencePermission]

    def get_queryset(self):
        return BeerReference.objects.all()