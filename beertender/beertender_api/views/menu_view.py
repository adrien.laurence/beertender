from beertender_api.models import BeerReference, Stock, Menu
from beertender_api.serializers import MenuSerializer
from rest_framework import filters
from rest_framework.viewsets import ReadOnlyModelViewSet


class MenuViewSet(ReadOnlyModelViewSet):
    serializer_class = MenuSerializer

    filter_backends = [filters.SearchFilter, filters.OrderingFilter]
    ordering_fields = ['ref', 'name', 'description']
    search_fields = ['ref', 'name', 'description']

    # permission_classes = [permissions.BeerReferencePermission]

    def get_queryset(self):
        queryset = []
        bar_counter_id = self.request.GET.get('bar_counter_id')
        is_available = self.request.GET.get('is_available')
        beer_references = BeerReference.objects.all()
        for ref in beer_references:
            if bar_counter_id is not None:
                stocks = Stock.objects.filter(beer_ref=ref.id, stock__gt=0, bar_counter=bar_counter_id)
            else:
                stocks = Stock.objects.filter(beer_ref=ref.id, stock__gt=0)
            menu = Menu()
            menu.beer_ref = ref
            menu.available = len(stocks) > 0
            if menu.available:
                queryset.append(menu)
            else:
                if not is_available or is_available != "True":
                    queryset.append(menu)

        return queryset
