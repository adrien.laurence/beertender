from beertender_api.models import Stock, BarCounter
from beertender_api.serializers.beer_reference_serializer import BeerReferenceSerializer
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer


class StockSerializer(ModelSerializer):

    ref = serializers.CharField(source="beer_ref.ref")
    name = serializers.CharField(source="beer_ref.name")
    description = serializers.CharField(source="beer_ref.description")

    class Meta:
        model = Stock
        fields = ['ref', 'name', 'description', 'stock']
