from beertender_api.models import BarCounter
from beertender_api.serializers.stock_serializer import StockSerializer
from rest_framework.serializers import ModelSerializer


class BarCounterSerializer(ModelSerializer):
    class Meta:
        model = BarCounter
        fields = ['pk', 'name']


class BarCounterStockSerializer(ModelSerializer):
    stocks = StockSerializer(many=True)

    class Meta:
        model = BarCounter
        fields = ['stocks']
