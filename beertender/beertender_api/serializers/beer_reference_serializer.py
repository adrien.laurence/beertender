from beertender_api.models import BeerReference
from rest_framework.serializers import ModelSerializer


class BeerReferenceSerializer(ModelSerializer):
    class Meta:
        model = BeerReference
        fields = ['ref', 'name', 'description']
