from beertender_api.models import Menu
from beertender_api.serializers.beer_reference_serializer import BeerReferenceSerializer
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, BaseSerializer


class MenuSerializer(BaseSerializer):

    def to_representation(self, instance):
        return {
            'ref': instance.beer_ref.ref,
            'name': instance.beer_ref.name,
            'description': instance.beer_ref.description,
            'available': instance.get_available_representation()
        }

    def get_available_representation(self):
        return "available" if self.available else "outofstock"