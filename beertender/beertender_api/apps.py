from django.apps import AppConfig


class BeertenderApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'beertender_api'
