from beertender_api.models.beer_reference import BeerReference
from django.db import models


class Menu(models.Model):
    available = models.fields.BooleanField()
    beer_ref = models.ForeignKey(BeerReference, null=False, on_delete=models.CASCADE)

    def get_available_representation(self):
        return "available" if self.available else "outofstock"
