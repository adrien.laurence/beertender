from django.db import models


class BeerReference(models.Model):
    ref = models.fields.CharField(max_length=100)
    name = models.fields.CharField(max_length=200)
    description = models.fields.TextField()
