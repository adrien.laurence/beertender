from django.db import models

from beertender_api.models.bar_counter import BarCounter
from beertender_api.models.beer_reference import BeerReference


class Stock(models.Model):
    stock = models.fields.IntegerField()
    beer_ref = models.ForeignKey(BeerReference, null=False, on_delete=models.CASCADE)
    bar_counter = models.ForeignKey(BarCounter, null=False, on_delete=models.CASCADE, related_name='stocks')
