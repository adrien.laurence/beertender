"""beertender URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from beertender_api.views import BarCounterViewSet, BeerReferenceViewSet, StockViewSet, UserViewSet, GroupViewSet, BarCounterStockViewSet, MenuViewSet
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

router = routers.SimpleRouter()

router.register(r'references', BeerReferenceViewSet, basename='references')
router.register(r'users', UserViewSet)
router.register(r'groups', GroupViewSet)
router.register(r'bars', BarCounterViewSet, basename='bars')
router.register(r'stock', BarCounterStockViewSet, basename='stock')
router.register(r'menu', MenuViewSet, basename='menu')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/', include(router.urls))
]
